<?php

/**
 *	=======================================================================
 *	SMART SUNSMART - DATA RETRIEVAL FOR UV INDEX & WEATHER FORECAST
 *	Data analysed based on BOM (Australian Bureau of Meteorology)'s
 *	3rd-party data channels (public domain resources)
 *
 *	## THIS FILE IS USED TO COLLECT REMOTE DATA TO LOCAL SERVER
 *	## CALLED BY SERVER'S CRON JOB - NOTHING TO DO WITH CLIENT REQ
 *	=======================================================================
 *
 *	@author Tris Le
 *	@version 1.1
 *	@copyright BirdBrain Logic 2012
 *
 *	LOG
 *	@since v1.1
 *	- No more heavy basing on remote ftp server, run a cron job to get data to local
 *	- Configure paths through index.php instead of the class file
 */

/* Define paths to be used throughout program life time */
define("APP_ROOT", dirname(__FILE__));
define("INTERNAL_DATA_UV", APP_ROOT . '\\data\\%s\\uv.txt');
define("INTERNAL_DATA_FC", APP_ROOT . '\\data\\%s\\fc.txt');
define("INTERNAL_DATA_ICO", 'data/ico/');
define("EXTERNAL_DATA_GRPH", '');
define("EXTERNAL_DATA_UV", '');
define("EXTERNAL_DATA_FC", '');
define("EXTERNAL_DATA_ICO", '');

/* Require the SmartSunSmart library and get ready */
require_once("library/smartsunsmart.php");

/*
 *	Check if there is any requested state or location
 *	if yes, get the requested, otherwise set default location to be WA's Perth
 */
$stateCode =	(isset($_POST['current-state']) && isset($_POST['current-location'])) 
				? $_POST['current-state'] : "WA";
$location  = 	(isset($_POST['current-state']) && isset($_POST['current-location']))
				? $_POST['current-location'] : "Perth";

/* Initialize a new Smart SunSmart object instance */
$sunSmart = new SmartSunSmart($stateCode, $location);

$sunSmart->setDefaultTimeZone("Australia", "Perth");

//$sunSmart->_debug_on(); //Uncomment to turn on debug Mode (activate print_r() for data manipulation)

/* Prepare to get the data */
$sunSmart->prepare();

/* Data retrieval, ready to call for view */
$data = array();
$data['state'] 			= $sunSmart->getState();
$data['location'] 		= $sunSmart->getLocation();
$data['graph'] 			= $sunSmart->getGraph();
$data['datUV'] 			= $sunSmart->getDatUV();
$data['datForecast'] 	= $sunSmart->getDatForecast();

/* Here, now, render the view, finish the work */
$sunSmart->out("view/smartsunsmart.tpl", $data);

?>