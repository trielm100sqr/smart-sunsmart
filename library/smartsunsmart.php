<?php

/**
 *	=======================================================================
 *	SMART SUNSMART - DATA RETRIEVAL FOR UV INDEX & WEATHER FORECAST
 *	Data analysed based on BOM (Australian Bureau of Meteorology)'s
 *	3rd-party data channels (public domain resources)
 *
 *	## THIS FILE IS USED TO COLLECT REMOTE DATA TO LOCAL SERVER
 *	## CALLED BY SERVER'S CRON JOB - NOTHING TO DO WITH CLIENT REQ
 *	=======================================================================
 *
 *	@author Tris Le
 *	@version 1.1
 *	@copyright BirdBrain Logic 2012
 *
 *	LOG
 *	@since v1.1
 *	- No more heavy basing on remote ftp server, run a cron job to get data to local
 *	- Configure paths through index.php instead of the class file
 */

class SmartSunSmart {

	/* State code i.e. WA, SYD, VIC, etc. */
	private $state;

	/* Location from the state above i.e. Perth, Sydney, etc. */
	private $location;

	/* UV Index data set to be retrieved */
	private $UVData = array();

	/* Weather forecast data set to be retrieved */
	private $foreCastData = array();

	/*
	 * Default path to UV data graph, note that file name is partly included and needs
	 * to be fully included correspondingly i.e. Perth_WA.png (%s_%s.png)
	 */
	private $remotePathToGraph = EXTERNAL_DATA_GRPH;

	/*
	 * Default path to UV data file, note that file name is partly included and needs
	 * to be fully included correspondingly i.e. IDYGP007.WA.txt (IDYGP007.%s.txt)
	 */
	private $remotePathToDatUV = EXTERNAL_DATA_UV;
	private $localPathToDatUV = INTERNAL_DATA_UV;

	/*
	 * Default path to weather forecast data file, note that file name is partly included
	 * and needs to be fully included correspondingly i.e. IDA00001.dat (IDA0000%d.dat)
	 */
	private $remotePathToDatForecast = EXTERNAL_DATA_FC;
	private $localPathToDatForecast = INTERNAL_DATA_FC;

	/*
	 * Default path to the weather forecast icons
	 */
	private $remotePathToForecastIcon = EXTERNAL_DATA_ICO;
	private $localPathToForecastIcon = INTERNAL_DATA_ICO;

	/* Use _debug_on() to set debug mode on for data manipulation */
	private $debug = false;

	/**
	 * Constructor
	 * 
	 * @param $stateCode code of the state e.g. WA for Western Australia
	 * @param $location location within the state e.g. Perth (in WA)
	 */
	public function __construct($stateCode, $location) {
		$this->setState($stateCode);
		$this->setLocation($location);
	}

	public static function specified() {
	}

	public function setDefaultTimeZone($country = 'Australia', $city = 'Perth') {
		date_default_timezone_set($country . '/' . $city);
	}

	/**
	 * setState() binds the requested state to the object
	 * @param $stateCode user requested state
	 */
	public function setState($stateCode) {
		$this->state = $stateCode;
	}

	/**
	 * setLocation() binds the requested location to the object
	 * @param $location user requested location
	 */
	public function setLocation($location = '') {
		$this->location = $location;
	}

	/**
	 * Replace a space character with an underscore character
	 * This is to get the graph from BOM through URL
	 *
	 * @param $string the string to be changed e.g. Halls Creek
	 * @return replaced string i.e. Halls_Creek
	 */
	private function spaceToUnderscore($string) {
		if (strpos($string, ' ') !== false) {
			$string = str_replace(' ', '_', $string);
		}
		return $string;
	}

	/**
	 * setPathToUVGraph() sets the path to the UV Graph via $this->location
	 */
	public function setPathToUVGraph() {
		$this->remotePathToGraph = sprintf($this->remotePathToGraph, $this->spaceToUnderscore($this->location), $this->state);
	}

	/**
	 * setPathToDatUV() sets the path to the UV data file via $this->state
	 */
	public function setPathToDatUV() {
		$this->localPathToDatUV = sprintf($this->localPathToDatUV, $this->state);
	}

	/**
	 * setPathToDatForecast() sets the path to the weather forecast data file via $this->state
	 */
	public function setPathToDatForecast() {
		$this->localPathToDatForecast = sprintf($this->localPathToDatForecast, $this->state);
	}

	/**
	 * Getter method getGraph()
	 * @return path to the UV graph, ready to render to view
	 */
	public function getGraph() {
		return $this->remotePathToGraph;
	}

	/**
	 * Entirely trim off unnecessary white space in a string
	 *
	 * @param $string to be trimmed
	 * @return trimmed string
	 */
	private function totalDataTrimming($string) {
		$string = trim($string);
		return preg_replace("/\s+/", " ", $string);
	}

	/**
	 * This is the core thing to get the UV data
	 * Parse the UV data from the text file when it's opened
	 * 
	 * @param $handle reference to the stream variable
	 */
	private function parseUVData(&$handle) {
		$location = $this->location;
		$dataNeeded = "";
		if ($handle) {
			// Read the text line by line until...
			while (($thisLine = fgets($handle)) != false) {
				// ... until the line that contains the requested location's data being caught
				if (strpos($thisLine, $location) !== false) {
					// Alright, get the needed data i.e. the whole line in the data text
					$dataNeeded = $thisLine;
					// Terminate the loop and start parsing the data
					break;
				}
			}
			fclose($handle);
		}

		// This is where the fun begins: parsing the data
		// So, if the data needed does exist...
		if ($dataNeeded != "") {
			// Trim off all the unnecessary space characters
			$dataNeeded = $this->totalDataTrimming($dataNeeded);

			// Extract the line of data using a white space character
			$extractData = explode(" ", $dataNeeded);

			/*=============================================
			@tris
			From now on, you will be seeing the indexes of the extractData array
			which seems to be very ambiguous. I can't do anything much about this
			since the BOM's data is given as a plain text file, so by extracting
			it, we are forced to use fixed indexes to get each desired datum.

			Tip: in order to see the whole array with indexing number, please try
			to turn on the debug mode, you will see it.
			==============================================*/

			// Sometimes the location has 2 words, so by getting one index only get
			// half of the location's name, this bit will deal with that issue.
			if (!is_numeric($extractData[4])) {
				// Combine the 2nd half of the location name to its first half
				$extractData[3] .= ' '. $extractData[4];
				// And then unset the 2nd half of the location name
				unset($extractData[4]);
				// Finally, sort the array after 4th element got combined to the 3rd element
				$extractData = array_values($extractData);
			}
			
			// If debug mode is on, fire the debug code
			if ($this->debug) {
				?><h1>Raw UV Index Data Array</h2><pre><?php print_r($extractData); ?></pre><?php
			}

			// Eventually, this is where the final data got retrieved for rendering the view
			$this->UVData['date'] = $extractData[4] . '/' . $extractData[5] . '/' .$extractData[6];
			$this->UVData['alertFrom'] = $extractData[10] . $extractData[11];
			$this->UVData['alertTo'] = $extractData[13] . substr($extractData[14], 0, -1);
			$this->UVData['maxUVIndex'] = $extractData[20];
			preg_match_all("^\[(.*?)\]^", $dataNeeded, $this->UVData['maxUVStatus'], PREG_PATTERN_ORDER);;
			$this->UVData['maxUVStatus'] = $this->UVData['maxUVStatus'][1][0];

			// If debug mode is on, fire the debug code
			if ($this->debug) {
				?><h1>Processed UV Index Data Array</h2><pre><?php print_r($this->UVData); ?></pre><?php
			}
		}
		else {
			echo $this->location . " was not found for " . $this->state . "'s data";
		}
	}

	/**
	 * Getter method getDatUV()
	 *
	 * @return all the UV data
	 */
	public function getDatUV() {
		$handle = @fopen($this->localPathToDatUV, "r");
		$this->parseUVData($handle);
		return $this->UVData;
	}

	/**
	 * Get the icon associated with the forecast status from BOM
	 * 
	 * @param $forecast the status of the current weather
	 * @return the path to the icon
	 */
	private function foreCastToIcon($forecast) {
		
		// Trim the trailing period (.) at the end of the word
		$forecast = substr($forecast, 0, -1);
		// Lowercasing $forecast
		$forecast = strtolower($forecast);

		if ($forecast == "sunny") {
			return $this->localPathToForecastIcon . "sunny.png";
		}
		elseif ($forecast == "mostly sunny") {
			return $this->localPathToForecastIcon . "partly-cloudy.png";
		}
		elseif (strpos($forecast, "shower") !== FALSE) {
			return $this->localPathToForecastIcon . "showers.png";
		}
		elseif (strpos($forecast, "rain") !== FALSE) {
			return $this->localPathToForecastIcon . "rain.png";
		}
		elseif (strpos($forecast, "clear") !== FALSE) {
			return $this->localPathToForecastIcon . "clear.png";
		}
		elseif ($forecast == "cloudy") {
			return $this->localPathToForecastIcon . "cloudy.png";
		}
		elseif ($forecast == "partly cloudy") {
			return $this->localPathToForecastIcon . "partly-cloudy.png";
		}
		elseif ($forecast == "hazy") {
			return $this->localPathToForecastIcon . "haze.png";
		}
		elseif ($forecast == "windy") {
			return $this->localPathToForecastIcon . "wind.png";
		}
		elseif ($forecast == "fog") {
			return $this->localPathToForecastIcon . "fog.png";
		}
		elseif ($forecast == "dusty") {
			return $this->localPathToForecastIcon . "dust.png";
		}
		elseif ($forecast == "frost") {
			return $this->localPathToForecastIcon . "frost.png";
		}
		elseif ($forecast == "snow") {
			return $this->localPathToForecastIcon . "snow.png";
		}
		elseif ($forecast == "storm") {
			return $this->localPathToForecastIcon . "storm.png";
		}
		else {
			return "";
		}
	}

	/**
	 * This is the core thing to get the Forecast data
	 * Parse the forecast data from the text file when it's opened
	 * 
	 * @param $handle reference to the stream variable
	 */
	private function parseForecastData(&$handle) {
		$location = $this->location;
		$dataNeeded = "";
		if ($handle) {
			// Read the text line by line until...
			while (($thisLine = fgets($handle)) != false) {
				// ... until the line that contains the requested location's data being caught
				if (strpos($thisLine, $location) !== false) {
					// Alright, get the needed data i.e. the whole line in the data text
					$dataNeeded = $thisLine;
					// Terminate the loop and start parsing the data
					break;
				}
			}
			fclose($handle);
		}

		// So, if the data needed does exist...
		if ($dataNeeded != "") {
			$foreCast = array();
			$data = explode("#", $dataNeeded);

			// Again, if you want to understand why these indexes, turn on the debug mode
			// in the index.php file, it'll show you.
			$foreCast['day_1'] = array(
									'min_t'		=>	$data[6],
									'max_t'		=>	$data[7],
									'forecast'	=>	$data[22],
									'icon'		=>	$this->foreCastToIcon($data[22])
								);
			$foreCast['day_2'] = array(
									'min_t'		=>	$data[8],
									'max_t'		=>	$data[9],
									'forecast'	=>	$data[23],
									'icon'		=>	$this->foreCastToIcon($data[23])
								);
			$foreCast['day_3'] = array(
									'min_t'		=>	$data[10],
									'max_t'		=>	$data[11],
									'forecast'	=>	$data[24],
									'icon'		=>	$this->foreCastToIcon($data[24])
								);
			$foreCast['day_4'] = array(
									'min_t'		=>	$data[12],
									'max_t'		=>	$data[13],
									'forecast'	=>	$data[25],
									'icon'		=>	$this->foreCastToIcon($data[25])
								);
			$foreCast['day_5'] = array(
									'min_t'		=>	$data[14],
									'max_t'		=>	$data[15],
									'forecast'	=>	$data[26],
									'icon'		=>	$this->foreCastToIcon($data[26])
								);
			$foreCast['day_6'] = array(
									'min_t'		=>	$data[16],
									'max_t'		=>	$data[17],
									'forecast'	=>	$data[27],
									'icon'		=>	$this->foreCastToIcon($data[27])
								);
			$foreCast['day_7'] = array(
									'min_t'		=>	$data[18],
									'max_t'		=>	$data[19],
									'forecast'	=>	$data[28],
									'icon'		=>	$this->foreCastToIcon($data[28])
								);

			// Binds the processed forecast data to the object
			$this->foreCastData = $foreCast;
		}

		// If debug mode is on, fire the debug code
			if ($this->debug) {
			?><h1>Processed Forecast Array</h2><pre><?php print_r($foreCast); ?></pre><?php
		}
	}

	/**
	 * Getter method getDatForecast()
	 *
	 * @return the forecast data ready to render to view
	 */
	public function getDatForecast() {
		$handle = @fopen($this->localPathToDatForecast, "r");
		$this->parseForecastData($handle);
		return $this->foreCastData;
	}

	/**
	 * Getter method getState()
	 *
	 * @return the state code e.g. WA, VIC, QLD, etc.
	 */
	public function getState() {
		return $this->state;
	}

	/**
	 * Getter method getLocation()
	 *
	 * @return the location name
	 */
	public function getLocation() {
		return $this->location;
	}

	/**
	 * It is what it sounds: prepare to get data
	 * i.e. setting all the paths
	 */
	public function prepare() {
		$this->setPathToUVGraph();
		$this->setPathToDatUV();
		$this->setPathToDatForecast();
	}

	/**
	 * Render the view with retrieved and processed data
	 *
	 * @param $template the template to be displayed
	 * @param $data the data to be included in the view
	 */
	public function out($template, $data) {
		include($template);
	}

	/**
	 * Turn on debug mode
	 */
	public function _debug_on() {
		$this->debug = true;
	}

	/**
	 * Clear off data, cheering guys.
	 */
	public function __destruct() {
		unset($this->UVData);
		unset($this->foreCastData);
	}

}

/* END OF LIBRARY */

?>