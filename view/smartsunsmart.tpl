<!DOCTYPE html>
<html>
<head>
<title>Smart SunSmart - BirdBrain Logic</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="window-target" content="_top" />
<link rel="stylesheet" type="text/css" href="view/style.css" />
<script type="text/javascript" src="js/thekit.js"></script>
</head>
<body id="body">
<div id="wrapper">
	<h1><?php echo $data['state'] . ' - ' . $data['location']; ?></h1>
	<div id="graph">
		<div id="graph-for" class="cf">
			<h4>Location</h4>
			<form action="" id="go-with-location" method="POST">
				<select id="state-selection" name="current-state">
					<option value="" selected="selected">Select State</option>
					<option value="NSW">New South Wales</option>
					<option value="NT">Northern Territory</option>
					<option value="QLD">Queensland</option>
					<option value="SA">South Australia</option>
					<option value="TAS">Tasmania</option>
					<option value="VIC">Victoria</option>
					<option value="WA">Western Australia</option>
				</select>
				<span id="load-location"></span>
			</form>
		</div>
		<h1 id="clientime"></h1>
		<div id="graph-image">
			<img src="<?php echo $data['graph']; ?>" />
		</div>
		<div id="data" class="cf">
			<div class="datum" style="float:left;width:35%">
				<h2>UV Data</h2>
				<ul>
					<li>Issue Date: <?php echo $data['datUV']['date']; ?></li>
					<li>
						UV Alert:
						<?php echo $data['datUV']['alertFrom']; ?>
						&nbsp;to&nbsp;
						<?php echo $data['datUV']['alertTo']; ?>
					</li>
					<li>Max UV: <?php echo $data['datUV']['maxUVIndex']; ?></li>
					<li>Status: <?php echo $data['datUV']['maxUVStatus']; ?></li>
				</ul>
			</div>
			<div class="datum" style="float:left;width:24%">
				<h2>Temperature</h2>
				<ul>
					<li>Min: 
						<?php echo ($data['datForecast']['day_1']['min_t'] != "") 
						? $data['datForecast']['day_1']['min_t'] 
						: (($data['datForecast']['day_2']['min_t'] != "")
							? $data['datForecast']['day_2']['min_t']
							: $data['datForecast']['day_3']['min_t']); ?>
					</li>
					<li>Max: 
						<?php echo ($data['datForecast']['day_1']['max_t'] != "") 
						? $data['datForecast']['day_1']['max_t'] 
						: (($data['datForecast']['day_2']['max_t'] != "")
							? $data['datForecast']['day_2']['max_t']
							: $data['datForecast']['day_3']['max_t']); ?>
					</li>
				</ul>
			</div>
			<div class="datum" style="float:left;width:24%">
				<h2>Forecast</h2>
				<?php if ($data['datForecast']['day_1']['forecast'] != ''): ?>
					<ul>
						<li>Currently: <?php echo $data['datForecast']['day_1']['forecast']; ?></li>
						<li><img src="<?php echo $data['datForecast']['day_1']['icon']; ?>" /></li>
					</ul>
				<?php else: ?>
					Not Available
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
</body>
</html>