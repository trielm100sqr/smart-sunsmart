/**
 *	=======================================================================
 *	SMART SUNSMART - DATA RETRIEVAL FOR UV INDEX & WEATHER FORECAST
 *	Data analysed based on BOM (Australian Bureau of Meteorology)'s
 *	3rd-party data channels (public domain resources)
 *
 *	## THIS FILE IS USED TO COLLECT REMOTE DATA TO LOCAL SERVER
 *	## CALLED BY SERVER'S CRON JOB - NOTHING TO DO WITH CLIENT REQ
 *	=======================================================================
 *
 *	@author Tris Le
 *	@version 1.1
 *	@copyright BirdBrain Logic 2012
 *
 *	LOG
 *	@since v1.1
 *	- No more heavy basing on remote ftp server, run a cron job to get data to local
 *	- Configure paths through index.php instead of the class file
 */
 
var clientTimeReaction = {
	register: function() {
		var currentTime = new Date()
		var hour = currentTime.getHours()
		var minute = currentTime.getMinutes()

		// Morning: 5am to 10am
		if (hour >= 5 && hour <= 10) {
			document.getElementById("clientime").innerHTML = 'Morning'
		}
		// Afternoon: 11am to 4pm
		else if (hour >= 11 && hour <= 16) {
			document.getElementById("clientime").innerHTML = 'Afternoon'
		}
		// Evening: 5pm to 10pm
		else if (hour >= 17 && hour <= 22) {
			document.getElementById("clientime").innerHTML = 'Evening'
		}
		// Night: 11am to 4am
		else {
			document.getElementById("clientime").innerHTML = 'Night'
		}
	}
}

window.onload = function() {

	// Get client's time and update the view accordingly
	clientTimeReaction.register();

	// When users select the State dropdown list
	var loadLocation = document.getElementById("state-selection");
	// Open another select dropdown list for location
	loadLocation.addEventListener('change', function(){
		var state = this.value;
		if (state == '') {
			document.getElementById("load-location").innerHTML = '';
		}
		// Load the list of locations in New South Wales
		else if (state == 'NSW') {
			document.getElementById("load-location").innerHTML = '<select name="current-location" id="location-selection"><option value="" selected="selected">Select Location</option><option value="Albury-Wodonga">Albury-Wodonga</option><option value="Armidale">Armidale</option><option value="Batemans Bay">Batemans Bay</option><option value="Bathurst">Bathurst</option><option value="Bourke">Bourke</option><option value="Bowral">Bowral</option><option value="Broken Hill">Broken Hill</option><option value="Cabramurra">Cabramurra</option><option value="Canberra">Canberra</option><option value="Cape Byron">Cape Byron</option><option value="Cessnock">Cessnock</option><option value="Charlotte Pass">Charlotte Pass</option><option value="Cobar">Cobar</option><option value="Coffs Harbour">Coffs Harbour</option><option value="Cooma">Cooma</option><option value="Deniliquin">Deniliquin</option><option value="Dubbo">Dubbo</option><option value="Forster">Forster</option><option value="Glen Innes">Glen Innes</option><option value="Gosford">Gosford</option><option value="Goulburn">Goulburn</option><option value="Grafton">Grafton</option><option value="Griffith">Griffith</option><option value="Inverell">Inverell</option><option value="Ivanhoe">Ivanhoe</option><option value="Katoomba">Katoomba</option><option value="Kempsey">Kempsey</option><option value="Lismore">Lismore</option><option value="Lithgow">Lithgow</option><option value="Lord Howe">Lord Howe</option><option value="Maitland">Maitland NSW</option><option value="Merimbula">Merimbula</option><option value="Moree">Moree</option><option value="Mudgee">Mudgee</option><option value="Narrabri">Narrabri</option><option value="Newcastle">Newcastle</option><option value="Norfolk Island">Norfolk Island</option><option value="Nowra">Nowra</option><option value="Orange">Orange</option><option value="Parkes">Parkes</option><option value="Perisher Valley">Perisher Valley</option><option value="Port Macquarie">Port Macquarie</option><option value="Scone">Scone</option><option value="Selwyn Snowfields">Selwyn Snowfields</option><option value="Singleton">Singleton</option><option value="Springwood">Springwood</option><option value="Sydney">Sydney</option><option value="Tamworth">Tamworth</option><option value="Taree">Taree</option><option value="Thredbo">Thredbo</option><option value="Tuggeranong">Tuggeranong</option><option value="Wagga Wagga">Wagga Wagga</option><option value="Wollongong">Wollongong</option><option value="Young">Young</option></select>';
		}
		// Load the list of locations in Northern Territory
		else if (state =='NT') {
			document.getElementById("load-location").innerHTML = '<select name="current-location" id="location-selection"><option value="" selected="selected">Select Location</option><option value="Alice Springs">Alice Springs</option><option value="Alyangula">Alyangula</option><option value="Borroloola">Borroloola</option><option value="Darwin">Darwin</option><option value="Jabiru">Jabiru</option><option value="Katherine">Katherine</option><option value="Lajamanu">Lajamanu</option><option value="Maningrida">Maningrida</option><option value="Nhulunbuy-Gove">Nhulunbuy-Gove</option><option value="Pirlangimpi">Pirlangimpi</option><option value="Tennant Creek">Tennant Creek</option><option value="Wadeye">Wadeye</option><option value="Yuendumu">Yuendumu</option><option value="Yulara">Yulara</option></select>';
		}
		// Load the list of locations in Queensland
		else if (state == 'QLD') {
			document.getElementById("load-location").innerHTML = '<select name="current-location" id="location-selection"><option value="" selected="selected">Select Location</option><option value="Amberley">Amberley</option><option value="Ayr-Home Hill">Ayr-Home Hill</option><option value="Barcaldine">Barcaldine</option><option value="Biloela">Biloela</option><option value="Boulia">Boulia</option><option value="Bowen">Bowen</option><option value="Brisbane">Brisbane</option><option value="Bundaberg">Bundaberg</option><option value="Burketown">Burketown</option><option value="Cairns">Cairns</option><option value="Charleville">Charleville</option><option value="Charters Towers">Charters Towers</option><option value="Clermont">Clermont</option><option value="Cloncurry">Cloncurry</option><option value="Cooktown">Cooktown</option><option value="Coolangatta">Coolangatta</option><option value="Dalby">Dalby</option><option value="Emerald">Emerald</option><option value="Gladstone">Gladstone</option><option value="Gold Coast">Gold Coast</option><option value="Goondiwindi">Goondiwindi</option><option value="Gympie">Gympie</option><option value="Hervey Bay">Hervey Bay</option><option value="Hughenden">Hughenden</option><option value="Ingham">Ingham</option><option value="Innisfail">Innisfail</option><option value="Ipswich">Ipswich</option><option value="Kingaroy">Kingaroy</option><option value="Longreach">Longreach</option><option value="Mackay">Mackay</option><option value="Mareeba">Mareeba</option><option value="Maryborough">Maryborough</option><option value="Moranbah">Moranbah</option><option value="Mt Isa">Mt Isa</option><option value="Normanton">Normanton</option><option value="Richmond">Richmond</option><option value="Rockhampton">Rockhampton</option><option value="Roma">Roma</option><option value="Sunshine Coast">Sunshine Coast</option><option value="Tewantin">Tewantin</option><option value="Thursday Island">Thursday Island</option><option value="Toowoomba">Toowoomba</option><option value="Townsville">Townsville</option><option value="Warwick">Warwick</option><option value="Weipa">Weipa</option><option value="Winton">Winton</option><option value="Yeppoon">Yeppoon</option></select>';
		}
		// Load the list of locations in South Australia
		else if (state == 'SA') {
			document.getElementById("load-location").innerHTML = '<select name="current-location" id="location-selection"><option value="" selected="selected">Select Location</option><option value="Adelaide">Adelaide</option><option value="Ceduna">Ceduna</option><option value="Clare">Clare</option><option value="Coober Pedy">Coober Pedy</option><option value="Coonawarra">Coonawarra</option><option value="Keith">Keith</option><option value="Kingscote">Kingscote</option><option value="Leigh Creek">Leigh Creek</option><option value="Maitland">Maitland SA</option><option value="Mount Gambier">Mount Gambier</option><option value="Murray Bridge">Murray Bridge</option><option value="Naracoorte">Naracoorte</option><option value="Nuriootpa">Nuriootpa</option><option value="Port Augusta">Port Augusta</option><option value="Port Lincoln">Port Lincoln</option><option value="Port Pirie">Port Pirie</option><option value="Renmark">Renmark</option><option value="Roxby Downs">Roxby Downs</option><option value="Victor Harbor">Victor Harbor</option><option value="Whyalla">Whyalla</option><option value="Woomera">Woomera</option><option value="Wudinna">Wudinna</option></select>';
		}
		// Load the list of locations in Tasmania
		else if (state == 'TAS') {
			document.getElementById("load-location").innerHTML = '<select name="current-location" id="location-selection"><option value="" selected="selected">Select Location</option><option value="Burnie">Burnie</option><option value="Campbell Town">Campbell Town</option><option value="Devonport">Devonport</option><option value="Flinders Island">Flinders Island</option><option value="Hobart">Hobart</option><option value="King Island">King Island</option><option value="Kingston">Kingston</option><option value="Launceston">Launceston</option><option value="Liawenee">Liawenee</option><option value="New Norfolk">New Norfolk</option><option value="Queenstown">Queenstown</option><option value="Scottsdale">Scottsdale</option><option value="Smithton">Smithton</option><option value="St Helens">St Helens</option><option value="Strahan">Strahan</option><option value="Swansea">Swansea</option><option value="Ulverstone">Ulverstone</option></select>';
		}
		// Load the list of locations in Victoria
		else if (state == 'VIC') {
			document.getElementById("load-location").innerHTML = '<select name="current-location" id="location-selection"><option value="" selected="selected">Select Location</option><option value="Albury-Wodonga">Albury-Wodonga</option><option value="Bairnsdale">Bairnsdale</option><option value="Ballarat">Ballarat</option><option value="Bendigo">Bendigo</option><option value="Colac">Colac</option><option value="Echuca">Echuca</option><option value="Falls Creek">Falls Creek</option><option value="Frankston">Frankston</option><option value="Geelong">Geelong</option><option value="Hamilton">Hamilton</option><option value="Horsham">Horsham</option><option value="LaTrobe Valley">LaTrobe Valley</option><option value="Laverton">Laverton</option><option value="Melbourne">Melbourne</option><option value="Mildura">Mildura</option><option value="Mornington">Mornington</option><option value="Mount Buller">Mount Buller</option><option value="Mount Dandenong">Mount Dandenong</option><option value="Mount Hotham">Mount Hotham</option><option value="Mt Baw">Mt Baw</option><option value="Orbost">Orbost</option><option value="Sale">Sale</option><option value="Scoresby">Scoresby</option><option value="Seymour">Seymour</option><option value="Shepparton">Shepparton</option><option value="Swan Hill">Swan Hill</option><option value="Tullamarine">Tullamarine</option><option value="Wangaratta">Wangaratta</option><option value="Warrnambool">Warrnambool</option><option value="Watsonia">Watsonia</option><option value="Wonthaggi">Wonthaggi</option><option value="Yarra Glen">Yarra Glen</option></select>';
		}
		// Load the list of locations in Western Australia
		else if (state == 'WA') {
			document.getElementById("load-location").innerHTML = '<select name="current-location" id="location-selection"><option value="" selected="selected">Select Location</option><option value="Albany">Albany</option><option value="Argyle">Argyle</option><option value="Bridgetown">Bridgetown</option><option value="Broome">Broome</option><option value="Bunbury">Bunbury</option><option value="Busselton">Busselton</option><option value="Carnarvon">Carnarvon</option><option value="Collie">Collie</option><option value="Dalwallinu">Dalwallinu</option><option value="Denham">Denham</option><option value="Derby">Derby</option><option value="Esperance">Esperance</option><option value="Eucla">Eucla</option><option value="Exmouth">Exmouth</option><option value="Fitzroy Crossing">Fitzroy Crossing</option><option value="Geraldton">Geraldton</option><option value="Halls Creek">Halls Creek</option><option value="Kalbarri">Kalbarri</option><option value="Kalgoorlie">Kalgoorlie</option><option value="Kalumburu">Kalumburu</option><option value="Karratha">Karratha</option><option value="Katanning">Katanning</option><option value="Kununurra">Kununurra</option><option value="Lake Grace">Lake Grace</option><option value="Leinster">Leinster</option><option value="Mandurah">Mandurah</option><option value="Manjimup">Manjimup</option><option value="Marble Bar">Marble Bar</option><option value="Margaret River">Margaret River</option><option value="Meekatharra">Meekatharra</option><option value="Merredin">Merredin</option><option value="Moora">Moora</option><option value="Morawa">Morawa</option><option value="Munjina">Munjina</option><option value="Narrogin">Narrogin</option><option value="Newman">Newman</option><option value="Northam">Northam</option><option value="Onslow">Onslow</option><option value="Pannawonica">Pannawonica</option><option value="Paraburdoo">Paraburdoo</option><option value="Perth">Perth</option><option value="Port Hedland">Port Hedland</option><option value="Ravensthorpe">Ravensthorpe</option><option value="Roebourne">Roebourne</option><option value="Tom Price">Tom Price</option><option value="Wyndham">Wyndham</option></select>';
		}
		// By default, load the list of locations in New South Wales
		else {
			document.getElementById("load-location").innerHTML = '<select name="current-location" id="location-selection"><option value="" selected="selected">Select Location</option><option value="Albury-Wodonga">Albury-Wodonga</option><option value="Armidale">Armidale</option><option value="Batemans Bay">Batemans Bay</option><option value="Bathurst">Bathurst</option><option value="Bourke">Bourke</option><option value="Bowral">Bowral</option><option value="Broken Hill">Broken Hill</option><option value="Cabramurra">Cabramurra</option><option value="Canberra">Canberra</option><option value="Cape Byron">Cape Byron</option><option value="Cessnock">Cessnock</option><option value="Charlotte Pass">Charlotte Pass</option><option value="Cobar">Cobar</option><option value="Coffs Harbour">Coffs Harbour</option><option value="Cooma">Cooma</option><option value="Deniliquin">Deniliquin</option><option value="Dubbo">Dubbo</option><option value="Forster">Forster</option><option value="Glen Innes">Glen Innes</option><option value="Gosford">Gosford</option><option value="Goulburn">Goulburn</option><option value="Grafton">Grafton</option><option value="Griffith">Griffith</option><option value="Inverell">Inverell</option><option value="Ivanhoe">Ivanhoe</option><option value="Katoomba">Katoomba</option><option value="Kempsey">Kempsey</option><option value="Lismore">Lismore</option><option value="Lithgow">Lithgow</option><option value="Lord Howe">Lord Howe</option><option value="Maitland NSW">Maitland NSW</option><option value="Merimbula">Merimbula</option><option value="Moree">Moree</option><option value="Mudgee">Mudgee</option><option value="Narrabri">Narrabri</option><option value="Newcastle">Newcastle</option><option value="Norfolk Island">Norfolk Island</option><option value="Nowra">Nowra</option><option value="Orange">Orange</option><option value="Parkes">Parkes</option><option value="Perisher Valley">Perisher Valley</option><option value="Port Macquarie">Port Macquarie</option><option value="Scone">Scone</option><option value="Selwyn Snowfields">Selwyn Snowfields</option><option value="Singleton">Singleton</option><option value="Springwood">Springwood</option><option value="Sydney">Sydney</option><option value="Tamworth">Tamworth</option><option value="Taree">Taree</option><option value="Thredbo">Thredbo</option><option value="Tuggeranong">Tuggeranong</option><option value="Wagga Wagga">Wagga Wagga</option><option value="Wollongong">Wollongong</option><option value="Young">Young</option></select>';
		}

		// When users select location within the selected state
		var applyLocation = document.getElementById("location-selection");
		// Submit the form if the selection is not the first option i.e. thd note
		applyLocation.addEventListener('change', function(e){
			if (e.value != "") {
				document.forms["go-with-location"].submit();
			}
		}, false);
	}, false);
}