<?php

/**
 *	=======================================================================
 *	SMART SUNSMART - DATA RETRIEVAL FOR UV INDEX & WEATHER FORECAST
 *	Data analysed based on BOM (Australian Bureau of Meteorology)'s
 *	3rd-party data channels (public domain resources)
 *
 *	## THIS FILE IS USED TO COLLECT REMOTE DATA TO LOCAL SERVER
 *	## CALLED BY SERVER'S CRON JOB - NOTHING TO DO WITH CLIENT REQ
 *	=======================================================================
 *
 *	@author Tris Le
 *	@version 1.1
 *	@copyright BirdBrain Logic 2012
 *
 *	LOG
 *	@since v1.1
 *	- No more heavy basing on remote ftp server, run a cron job to get data to local
 *	- Configure paths through index.php instead of the class file
 */

/* Define paths to be used throughout program life time */
define("APP_ROOT", dirname(__FILE__));
define("INTERNAL_DATA_UV", APP_ROOT . '\\data\\%s\\uv.txt');
define("INTERNAL_DATA_FC", APP_ROOT . '\\data\\%s\\fc.txt');
define("EXTERNAL_DATA_UV", '');
define("EXTERNAL_DATA_FC", '');

$stateList = array(
					'2'	=>	'NSW',
					'3'	=>	'VIC',
					'4'	=>	'QLD',
					'5'	=>	'SA',
					'6'	=>	'WA',
					'7'	=>	'TAS',
					'8'	=>	'NT',
				);

/* Start downloading remote data files to local data files, state by state */
foreach ($stateList as $code => $state) {
	$remoteUVDataFile = sprintf(EXTERNAL_DATA_UV, $state);
	$remoteForecastDataFile = sprintf(EXTERNAL_DATA_FC, intval($code));
	$localUVDataFile = sprintf(INTERNAL_DATA_UV, $state);
	$localforecastDataFile = sprintf(INTERNAL_DATA_FC, $state);
	file_put_contents($localUVDataFile, file_get_contents($remoteUVDataFile));
	file_put_contents($localforecastDataFile, file_get_contents($remoteForecastDataFile));
}

?>